
Java8 Features:
================
1. Functional Interface  ---> java.util.function.*
2. Lambda Expressions
3. Interfaces can have static and default methods 
4. Method Reference and Constructor Reference
5. Stream API 
6. Optional<T>  type
7. Comparator Functional Interface
8. Collectors class


Important:
============

1. String , StringBuffer API 
2. OOPs 
3. Arrays and Collections
4. java.io File Read , Write
5. Concurrent Collection API
6. Multi threading
7. Java8 
8. Exception Handling 
